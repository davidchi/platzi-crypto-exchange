Vue.component('modal', {
    data() {
        return {
            title: 'Componente Modal',
        }
    },

    methods: {
        triggerClose() {
            this.$emit('close');
        }
    },

    template: `
    <div class="modal-mask">
        <div class="modal-wrapper">
            <div class="modal-container">
                <h3>{{title}}</h3>
                <div>
                    <slot name="body"></slot>
                </div>
                <footer>
                    <button v-on:click="triggerClose">Cerrar</button>
                </footer>
            </div>
        </div>
    </div>
    `
})

var app = new Vue({
    el: '#app',
    data() {
        return {
            showModal: true
        }
    },

    methods: {
        toggleModal() {
            this.showModal = !this.showModal;
        },
    }
})