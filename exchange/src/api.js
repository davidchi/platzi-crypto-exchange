const url = "https://api.coincap.io/v2";

function getAssets() {
  return fetch(`${url}/assets?limit=20`)
    .then((response) => response.json())
    .then((response) => response.data);
}

function getAsset(coin) {
  return fetch(`${url}/assets/${coin}`)
    .then((response) => response.json())
    .then((response) => response.data);
}

function getAssetHistory(coin) {
  return fetch(`${url}/assets/${coin}/history?interval=d1`)
    .then((res) => res.json())
    .then((res) => res.data);
}

// TODO handle bad request to exchange route because of bad exchangeId names for certain exchanges (ex. Bitcoin.com, Coinbase Pro)
function getMarkets(coin) {
  return fetch(`${url}/assets/${coin}/markets?limit=5`)
    .then((res) => res.json())
    .then((res) => res.data);
}

function getExchange(id) {
  return fetch(`${url}/exchanges/${id}`)
    .then((res) => res.json())
    .then((res) => res.data);
}

export default {
  getAssets,
  getAsset,
  getAssetHistory,
  getMarkets,
  getExchange,
};
