Vue.component('CoinDetail', {
    props: ['coin'],

    data() {
        return {
            showPrices: false,
            value: 0,
        }
    },

    methods: {
        toogleShowPrices() {
            this.showPrices = !this.showPrices;
            this.$emit('change-color', this.showPrices ? 'FF96C8' : '3D3D3D')
        }
    },

    computed: {
        title() {
            return `${this.coin.name} - ${this.coin.symbol}`
        },

        convertedValue() {
            if (!this.value) {
                return 0;
            }

            return this.value / this.coin.price;
        }
    },

    created() {
        console.log('Created CoinDetail...')
    },

    mounted() {
        console.log('Mounted CoinDetail...')
    },

    template: `
    <div>
        <img
            class="pointer"
            v-on:mouseover="toogleShowPrices"
            v-on:mouseout="toogleShowPrices"
            v-bind:src="coin.img"
            v-bind:alt="coin.name"
        />

        <h1 v-bind:class="coin.changePercent > 0 ? 'green' : 'red'">
            {{title}}
            <span v-if="coin.changePercent > 0">👍</span>
            <span v-else-if="coin.changePercent < 0">👎</span>
            <span v-else>🤞</span>
            <span class="pointer" v-on:click="toogleShowPrices">
                {{showPrices ? '🙈' : '🐵'}}
            </span>
        </h1>

        <input type="number" v-model="value" />
        <span>{{convertedValue}} BTC</span>

        <slot name="text"></slot>
        <slot name="link"></slot>

        <ul v-show="showPrices">
            <li
                class="uppercase"
                v-bind:class="{orange: priceObj.value == coin.price, red: priceObj.value < coin.price, green: priceObj.value > coin.price}"
                v-for="(priceObj, index) in coin.pricesWithDays"
                v-bind:key="priceObj.day"
            >
                {{index}} - {{priceObj.day}} - {{priceObj.value}}
            </li>
        </ul>
    </div>
    `
})

var app = new Vue({
    el: '#app',
    data() {
        return {
            btc: {
                name: 'Bitcoin',
                symbol: 'BTC',
                img: 'https://cryptologos.cc/logos/thumbs/bitcoin.png?v=013',
                changePercent: 0,
                price: 8400,
                pricesWithDays: [
                    { day: 'Lunes', value: 8400 },
                    { day: 'Martes', value: 7900 },
                    { day: 'Miercoles', value: 8200 },
                    { day: 'Jueves', value: 9000 },
                    { day: 'Viernes', value: 9400 },
                    { day: 'Sabado', value: 10000 },
                    { day: 'Domingo', value: 10200 },
                ],
            },
            color: '3D3D3D',
        }
    },

    created() {
        console.log('Created...')
    },

    mounted() {
        console.log('Mounted...')
    },

    methods: {
        updateColor(color) {
            this.color = color || this.color.split('').reverse().join('')
        }
    }
})