var app = new Vue({
    el: '#app',
    data: {
        courses: [],
        title: '',
        time: '',
    },
    // Computed are computed PROPERTIES, that can change dynamically
    computed: {
        totalTime: function () {
            var total = 0;
            if (this.courses.length > 0) {
                this.courses.forEach(course => total += parseInt(course.time))
            }
            return total;
        }
    },
    // Methods are simple methods that can be called anytime from the v directives
    methods: {
        addCourse: function () {
            // Si los datos del usuario son validos
            if (this.title != '' && this.time >= 0) {
                // Si el curso no ha sido registrado
                const matchAny = (course) => course.title == this.title
                if (this.courses.some(matchAny)) {
                    alert("Este curso ya ha sido registrado")
                } else {
                    this.courses.push({ title: this.title, time: this.time })
                }

            } else {
                alert("Error guardando curso, ensaya de nuevo...")
            }
        },

        removeCourse: function (index) {
            this.courses.splice(index, 1)
        }
    },
})