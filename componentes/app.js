Vue.component('counter', {
    data() {
        return {
            counter: 0,
        }
    },

    methods: {
        increment: function () {
            this.counter += 1
        }
    },

    template: `
        <div>
            <button v-on:click="increment"> Click me! </button>
            <span> {{counter}} </span>
        </div>
    `
})

var app = new Vue({
    el: '#app',
    data: {
        title: 'Hola'
    }
})